import PropTypes from 'prop-types';
import '../assets/styles/Message.css';

function Message({ status, from, text, date }) {
    return (
        <div className='container'>
            <div className='root'>
                <div>
                    <h4>{from}</h4>
                    <h5>{text}</h5>
                    <small>{status}</small>
                </div>
                <div>{date.toString()}</div>
            </div>
            <hr className='divider' />
        </div>
    )
}

Message.propTypes = {
    status: PropTypes.string,
    text: PropTypes.string,
    date: PropTypes.any,
    from: PropTypes.string,
}

Message.defaultProps = {
    status: '',
    text: '',
    date: new Date(),
    from: '',
}

export default Message;
