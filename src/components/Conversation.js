import '../assets/styles/Conversation.css';
import { Link } from "react-router-dom";
import PropTypes from 'prop-types';

function Conversation({contact, lastMessage, date, conversationId}){

    return (
        <Link to={`/messages/${conversationId}`}>
            <div className="container">
                <div className="root">
                    <div>
                        <h4>{contact}</h4>
                        <h5>{lastMessage}</h5>
                    </div>
                    <div>{date.toString()}</div>
                </div>
                <hr className="divider" />
            </div>
        </Link>
    )

}

Conversation.propTypes = {
    contact: PropTypes.string,
    lastMessage: PropTypes.string,
    date: PropTypes.any,
    conversationId: PropTypes.number,
}

Conversation.defaultProps = {
    contact: '',
    lastMessage: '',
    date: new Date(),
    conversationId: 0,
}

export default Conversation;