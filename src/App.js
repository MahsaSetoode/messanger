
import './App.css';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import ConversationList from './pages/ConversationList';
import Profile from './pages/Profile';
import Messages from './pages/Messages';

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/conversation" exact component={ConversationList} />
          <Route path="/profile" exact component={Profile} />
          <Route path="/messages/:conversationId" exact component={Messages} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
