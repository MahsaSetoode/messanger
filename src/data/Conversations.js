const Conversation = [
    {
        fullName: 'Mahsa Setoode',
        lastMessage: 'Ok, I see u.',
        date: new Date()
    },
    {
        fullName: 'Mom',
        lastMessage: 'Are you free?',
        date: new Date()
    },
    {
        fullName: 'Ala Farokhi',
        lastMessage: '<3',
        date: new Date()
    },
    {
        fullName: 'Adib Azizi',
        lastMessage: 'when could I callback?',
        date: new Date()
    }
]
export default Conversation;