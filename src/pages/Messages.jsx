import { Button } from "@material-ui/core";
import { useEffect, useState } from "react"
import Message from "../components/Message";
import { getMessages } from "../functions/api";


function Messages({ match }) {

    const [loadMore, setLoadMore] = useState(false);
    const [messages, setMessages] = useState();
    const [conversationId, setConversationid] = useState(match.params.conversationId);

    useEffect(async function () {
        const apiResult = await getMessages(conversationId);
        setMessages(apiResult.data);
    }, [])

    const changeLoadMore = () => {
        setLoadMore(!loadMore);
    }

    return (
        <div style={{margin:"1.5 er"}}>
            {messages && Array.isArray(messages) && messages.slice(0, loadMore ? messages.length : 3).map((message, index) => (
                <Message
                    key={index}
                    date={message.date}
                    text={message.text}
                    status={message.status}
                    from={message.from}
                />
            ))}
            <Button onClick={changeLoadMore}>{loadMore ? 'Show Less' : 'Show More'}</Button>
        </div>
    )
}

export default Messages;
