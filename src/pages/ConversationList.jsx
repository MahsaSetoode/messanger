
import { useEffect, useState } from "react";
import Conversation from "../components/Conversation";
import { getConversations } from "../functions/api";

function ConversationList(){
    const [userId, setUserId] = useState(localStorage.getItem('userId'));
    const [conversatons, setConevrsations] = useState();

    useEffect(async function () {
        const result = await getConversations(userId);
        setConevrsations(result && result.data);
    }, []);

    const styles = {
        container: {
            marginLeft: '5rem',
            marginRight: '5rem',
            display: 'flex',
            flexDirection: 'column',
            overflowY: 'auto',
        },
    }

    return (
        <div style={styles.container}>
            {conversatons && Array.isArray(conversatons) && conversatons.map((conversation, index) => (
                <Conversation
                    key={index}
                    conversationId={conversation._id}
                    contact={conversation.toName}
                    date={conversation.lastMessageDate}
                    lastMessage={conversation.lastMessageText}
                />
            ))}
        </div>
    )

}

export default ConversationList;