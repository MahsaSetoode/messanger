import axios from "axios";

export const apiURL = 'http://localhost:3001/api/';

export async function getConversations(userId) {
    try {
        const conversations = await axios.get(apiURL + "conversation/all");
        console.log(conversations)
        return conversations.data;
    } catch (error) {
        console.error(error);
    }
}

export async function getMessages(conversationId) {
    try {
        const messages = await axios.get(apiURL + "message/" + conversationId);
        return messages.data;
    } catch (error) {
        console.error(error);
    }
}
